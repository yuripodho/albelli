﻿using Albelli.Application.Services;
using Albelli.Infra.Entities;
using Albelli.Infra.Interfaces;
using Albelli.Infra.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Api.Tests
{
    [TestClass]
    public class CustomerTest
    {
        CustomerRepository customerRepository = new CustomerRepository();

        [TestMethod]
        public void Add()
        {
            Customer customer = new Customer();
            customer.Email = DataTest.CustomerTestEmail;
            customer.Name = DataTest.CustomerTestName; ;

            customerRepository.Add(customer);

            Assert.AreNotEqual(0, customer.Id);
        }

        [TestMethod]
        public void FindCustomerByEmail()
        {
            var customer = this.customerRepository.GetByEmail(DataTest.TestEmail);
            Assert.IsNotNull(customer);
        }

        [TestMethod]
        public void FindCustomersNoOrder()
        {
            var customers = this.customerRepository.GetAll();

            foreach(var customer in customers)
            {
                Assert.AreEqual(null, customer.Orders);
            }
        }

        [TestMethod]
        public void FindCustomerWithOrder()
        {
            var customer = customerRepository
                           .GetAllWithOrders()
                           .Where(x => x.Email == DataTest.TestEmail)
                           .FirstOrDefault();

            Assert.AreNotEqual(null, customer.Orders);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void AddExistingEmail()
        {
            CustomerService service = new CustomerService(customerRepository);

            var customer = new Customer();
            customer.Email = DataTest.TestEmail;
            customer.Name = "Yuri";

            service.Add(customer);

        }

    }
}
