﻿using Albelli.Infra.Entities;
using Albelli.Infra.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Api.Tests.Tests
{
    [TestClass]
    public class OrderTest
    {
        CustomerRepository customerRepository = new CustomerRepository();
        OrderRepository orderRepository = new OrderRepository();

        [TestMethod]
        public void CreateOrder()
        {
            var customer = customerRepository.GetByEmail(DataTest.TestEmail);

            var order = new Order();
            order.CreatedDate = DateTime.Now;
            order.Price = 200;
            order.CustomerId = customer.Id;

            orderRepository.Add(order);

            Assert.AreNotEqual(0, order.Id);
        }

    }
}
