﻿using Albelli.Application.Services;
using Albelli.Domain.Interfaces;
using Albelli.Infra.Entities;
using Albelli.Infra.Interfaces;
using Albelli.Infra.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Albelli.Api.Tests
{
    [TestClass]
    public class Startup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            CustomerRepository customerRespository = new CustomerRepository();
            OrderRepository orderRepository = new OrderRepository();

            Customer customer = new Customer();
            customer.Email = DataTest.TestEmail;
            customer.Name = DataTest.TestName;
            customerRespository.Add(customer);

            Order order = new Order();
            order.CreatedDate = DateTime.Now;
            order.CustomerId = customer.Id;
            order.Price = 200;
            orderRepository.Add(order);
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            CustomerRepository customerRepository = new CustomerRepository();

            var customer = customerRepository.GetByEmail(DataTest.TestEmail);
            if(customer != null)
                customerRepository.Delete(customer);

            customer = customerRepository.GetByEmail(DataTest.CustomerTestEmail);
            if (customer != null)
                customerRepository.Delete(customer);
        }


    }
}
