﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Albelli.Infra.Context;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Albelli.Infra.Base
{
    public class Repository<TEntity> : IDisposable,
         IRepository<TEntity> where TEntity : class
    {
        public DatabaseContext ctx = new DatabaseContext();
        public IQueryable<TEntity> GetAll()
        {
            return ctx.Set<TEntity>();
        }

        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return GetAll().Where(predicate).AsQueryable();
        }

        public TEntity Find(params object[] key)
        {
            return ctx.Set<TEntity>().Find(key);
        }

        public void Update(TEntity obj)
        {
            ctx.Entry(obj).State = EntityState.Modified;

        }

        public void Add(TEntity obj)
        {
            using (var db = new DatabaseContext())
            {
               db.Set<TEntity>().Add(obj);
               db.SaveChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            using (var db = new DatabaseContext())
            {
                ctx.Entry(entity).State = EntityState.Deleted;

                ctx.SaveChanges();
            }
        }

        public void Delete(Func<TEntity, bool> predicate)
        {
            using (var db = new DatabaseContext())
            {
                ctx.Set<TEntity>()
                    .Where(predicate).ToList()
                    .ForEach(del => ctx.Set<TEntity>().Remove(del));
            }
        }

        public void Dispose()
        {
            using (var db = new DatabaseContext())
            {
                ctx.Dispose();
            }
        }

    }
}
