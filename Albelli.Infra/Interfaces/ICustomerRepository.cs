﻿using Albelli.Infra.Base;
using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Infra.Interfaces
{
    //public interface ICustomerRepository : IRepository<Customer>
    public interface ICustomerRepository : IRepository<Customer>
    {
        IEnumerable<Customer> GetAllWithOrders();

        Customer GetByEmail(string email);

        Customer GetById(int id);
    }
}
