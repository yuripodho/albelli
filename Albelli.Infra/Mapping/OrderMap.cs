﻿using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Infra.Mapping
{
    public class OrderMap
    {
        public OrderMap(EntityTypeConfiguration<Order> entity)
        {
            entity.ToTable("order");

            entity.HasKey(x => x.Id);

            entity.Property(c => c.CustomerId)
                  .HasColumnName("customer_id");

            entity.Property(o => o.CreatedDate)
                  .HasColumnName("created_date");
        }
    }
}
