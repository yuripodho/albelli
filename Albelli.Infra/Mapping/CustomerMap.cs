﻿using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Infra.Mapping
{
    public class CustomerMap
    {

        public CustomerMap(EntityTypeConfiguration<Customer> entity)
        {
            entity.ToTable("customer");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.Name).HasColumnName("name");

            entity.HasMany(c => c.Orders)
                  .WithOptional()
                  .HasForeignKey(x => x.CustomerId)
                  .WillCascadeOnDelete();

        }

    }
}
