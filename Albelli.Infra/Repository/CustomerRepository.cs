﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Albelli.Infra.Base;
using Albelli.Infra.Entities;
using Albelli.Infra.Repository;
using Albelli.Infra.Interfaces;
using Albelli.Infra.Context;

namespace Albelli.Infra.Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository 
    {

        public IEnumerable<Customer> GetAllWithOrders()
        {
            
            var customers = base.ctx.Customers.Include("Orders").ToList();

            // customers.ForEach(x => x.Orders = _orderRepository.GetOrdersByCustomer(x.Id));

            return customers;
        }

        public Customer GetByEmail(string email)
        {
            using (var db = new DatabaseContext())
            {
                return db.Customers.Where(x => x.Email == email).FirstOrDefault();
            }
        }

        public Customer GetById(int id)
        {
            return base.ctx.Customers.Include("Orders").AsNoTracking().Where(x => x.Id == id).FirstOrDefault();
        }

     
    }
}
