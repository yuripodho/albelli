﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Albelli.Infra.Base;
using Albelli.Infra.Entities;
using Albelli.Domain.Interfaces;

namespace Albelli.Infra.Repository
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public IEnumerable<Order> GetOrdersByCustomer(int customerId)
        {
            return ctx.Orders.Where(x => x.CustomerId == customerId);

        }

    }
}
