﻿using Albelli.Infra.Entities;
using Albelli.Infra.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Infra.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=DefaultConnection") { }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            new CustomerMap(modelBuilder.Entity<Customer>());
            new OrderMap(modelBuilder.Entity<Order>());

            modelBuilder.Build(base.Database.Connection);
        }
    }
}
