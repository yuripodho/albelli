﻿using Albelli.Application.Services;
using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace Albelli.Api.Controllers
{
    [RoutePrefix("customer")]
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Route("list")]
        public IEnumerable<Customer> List()
        {
           return _customerService.GetAll();
        }

        [HttpGet]
        [Route("list/orders")]
        public IEnumerable<Customer> ListWithOrders()
        {
           return _customerService.GetAllWithOrders();
        }

        [HttpPost]
        [Route("add")]
        public Customer Add(Customer model)
        {
           return _customerService.Add(model);
        }

        [HttpGet]
        [Route("{id}")]
        public Customer Get(int id)
        {
            return _customerService.GetById(id);
        }

    }
}
