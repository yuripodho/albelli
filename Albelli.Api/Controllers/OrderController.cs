﻿using Albelli.Application.Services;
using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace Albelli.Api.Controllers
{
    [RoutePrefix("order")]
    public class OrderController : ApiController
    {
        protected readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        [Route("add")]
        public Order Add(Order model)
        {
            return _orderService.CreateOrder(model);
        }

    }
}