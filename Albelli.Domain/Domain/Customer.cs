﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Domain
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public IEnumerable<Order> Orders { get; set; }

        public bool IsValid()
        {
            if(string.IsNullOrEmpty(Email))
            {
                throw new ValidationException("Email should not be empty");
            }

            if (string.IsNullOrEmpty(Name))
            {
                throw new ValidationException("Name should not be empty");
            }

            return true;
        }
    }
}
