﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Domain.Domain
{
    public class Order
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }

        public bool IsValid()
        {
            if(CustomerId == Guid.Empty)
            {
                throw new ValidationException("Customer Id must be informed");
            }

            return true;
        }
        
    }
}
