﻿using System;
using Albelli.Domain.Interfaces;
using Albelli.Infra.Entities;

namespace Albelli.Application.Services
{
    public class OrderService : IOrderService
    {
        protected readonly ICustomerService _customerService;
        protected readonly IOrderRepository _orderRepository;

        public OrderService(ICustomerService customerService, IOrderRepository orderRepository)
        {
            _customerService = customerService;
            _orderRepository = orderRepository;
        }


        public Order CreateOrder(Order order)
        {
            var customer = _customerService.GetById(order.CustomerId);

            if (customer == null) throw new Exception("Customer not found");

            order.CreatedDate = DateTime.Now;

            _orderRepository.Add(order);

            return order;
        }
    }
}
