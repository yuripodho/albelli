﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Albelli.Infra.Interfaces;
using Albelli.Infra.Entities;

namespace Albelli.Application.Services
{
    public class CustomerService : ICustomerService
    {
        protected readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public Customer Add(Customer customer)
        {
            if (GetByEmail(customer.Email) != null) throw new ValidationException("User already exist");

            _customerRepository.Add(customer);

            return customer;
        }

        public IEnumerable<Customer> GetAll()
        {
            return _customerRepository.GetAll();
        }

        public IEnumerable<Customer> GetAllWithOrders()
        {
            return _customerRepository.GetAllWithOrders();
        }

        public Customer GetByEmail(string email)
        {
            return _customerRepository.GetByEmail(email);
        }

        public Customer GetById(int id)
        {
            return _customerRepository.GetById(id);
        }
    }
}
