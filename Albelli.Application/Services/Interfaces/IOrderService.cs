﻿using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Application.Services
{
    public interface IOrderService
    {
        Order CreateOrder(Order model);
    }
}
