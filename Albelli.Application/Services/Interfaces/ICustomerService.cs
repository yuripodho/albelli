﻿using Albelli.Domain;
using Albelli.Infra.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Application.Services
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        IEnumerable<Customer> GetAllWithOrders();
        Customer GetByEmail(string email);
        Customer GetById(int id);
        Customer Add(Customer customer);
    }
}
